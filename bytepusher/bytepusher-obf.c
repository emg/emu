#include <stdio.h>
#include <SDL2/SDL.h>
#define T e.type
#define K e.key.keysym.sym
#define S(x) SDL_##x
#define D(x) SDLK_##x,
#define U SDL_WINDOWPOS_UNDEFINED
S(Keycode)k[]={D(x)D(1)D(2)D(3)D(q)D(w)D(e)D(a)D(s)D(d)D(z)D(c)D(4)D(r)D(f)D(v)}
;S(Window)*w;S(Renderer)*r;S(Texture)*t;S(AudioDeviceID)a;uint8_t m[16777224],*p;
int main(void){int W=256,N=16,i,c;uint32_t l[W],b,x[W*W],*o;for(i=0;i<6;i++)for(
c=0;c<6;c++)for(b=0;b<6;b++)l[i*36+c*6+b]=i*51<<N|c*51<<8|b*51;for(i=0;(c=
getchar())!=EOF&&i<16777224;m[i++]=c);if(atexit(S(Quit))||S(Init)(S(INIT_VIDEO)|
S(INIT_AUDIO)))exit(1);w=S(CreateWindow)("bytepusher",U,U,W*3,W*3,S(WINDOW_SHOWN
));r=S(CreateRenderer)(w,-1,0);t=S(CreateTexture)(r,S(PIXELFORMAT_BGRA32),S(
TEXTUREACCESS_STATIC),W,W);a=S(OpenAudioDevice)(0,0,&(S(AudioSpec)){.freq=15360,
.format=AUDIO_S8,.channels=1,.samples=W},0,S(AUDIO_ALLOW_ANY_CHANGE));S(
PauseAudioDevice)(a,0);if(!w|!r|!t|!a)exit(1);for(;;){b=*m<<8|m[1];S(Event)e;
while(S(PollEvent)(&e)){if(T!=S(KEYDOWN)&&T!=S(KEYUP)&&T!=S(QUIT))continue;if(T
==S(QUIT)||K==SDLK_ESCAPE)exit(0);for(i=0;i<N;i++)if(K==k[i])b=(b&~(1<<i))|(T==S
(KEYDOWN))<<i;}*m=b>>8;m[1]=b&255;p=m+(m[2]<<N|m[3]<<8|m[4]);for(i=0;i<65536;i++
)m[p[3]<<N|p[4]<<8|p[5]]=m[*p<<N|p[1]<<8|p[2]],p=m+(p[6]<<N|p[7]<<8|p[8]);while(
S(GetQueuedAudioSize)(a)>512)S(Delay)(1);S(QueueAudio)(a,m+(m[6]<<N|m[7]<<8),W);
for(p=m+(m[5]<<N),o=x;o<x+W*W;*o++=l[*p++]);S(UpdateTexture)(t,0,x,W*4);S(
RenderClear)(r);S(RenderCopy)(r,t,0,0);S(RenderPresent)(r);}}
