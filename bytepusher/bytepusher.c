#include <stdio.h>
#include <SDL2/SDL.h>

enum { width = 256, height = 256, scale = 3, format = SDL_PIXELFORMAT_BGRA32, };
static SDL_AudioSpec audiospec =
	{ .freq = 15360, .format = AUDIO_S8, .channels = 1, .samples = 256, };
static SDL_Keycode keys[] = { /* keys[original] = remapped */
	SDLK_x, SDLK_1, SDLK_2, SDLK_3, SDLK_q, SDLK_w, SDLK_e, SDLK_a,
	SDLK_s, SDLK_d, SDLK_z, SDLK_c, SDLK_4, SDLK_r, SDLK_f, SDLK_v,
};
static SDL_Window *win;
static SDL_Renderer *ren;
static SDL_Texture *tex;
static SDL_AudioDeviceID aud;
static uint8_t ram[16*1024*1024 + 8];
static uint32_t colormap[256];

static int
cycle(void)
{
	/* Check input */
	uint16_t keybits = ram[0]<<8 | ram[1];
	SDL_Event ev;
	while (SDL_PollEvent(&ev)) {
		if (ev.type != SDL_KEYDOWN && ev.type != SDL_KEYUP && ev.type != SDL_QUIT)
			continue;
		if (ev.type == SDL_QUIT || ev.key.keysym.sym == SDLK_ESCAPE)
			return 1;
		for (size_t i = 0; i < sizeof(keys)/sizeof(*keys); i++)
			if (ev.key.keysym.sym == keys[i])
				keybits = (keybits & ~(1 << i)) | (ev.type == SDL_KEYDOWN)<<i;
	}
	ram[0] = keybits >> 8;
	ram[1] = keybits & 0xFF;

	/* Run instructions */
	uint8_t *pc = ram + (ram[2]<<16 | ram[3]<<8 | ram[4]);
	for (int i = 0; i < 65536; i++) {
		ram[pc[3]<<16 | pc[4]<<8 | pc[5]] = ram[*pc<<16 | pc[1]<<8 | pc[2]];
		pc = ram + (pc[6]<<16 | pc[7]<<8 | pc[8]);
	}

	/* Wait until audio has nearly run out, use this as our clock */
	while (SDL_GetQueuedAudioSize(aud) > 2*audiospec.samples)
		SDL_Delay(1);

	/* Play audio */
	SDL_QueueAudio(aud, ram + (ram[6]<<16 | ram[7]<<8), audiospec.samples);

	/* Draw video */
	uint8_t *in = ram + (ram[5]<<16);
	uint32_t pixels[width*height], *out = pixels;
	while (out < pixels + width*height)
		*out++ = colormap[*in++];
	SDL_UpdateTexture(tex, 0, pixels, width*SDL_BYTESPERPIXEL(format));
	SDL_RenderClear(ren);
	SDL_RenderCopy(ren, tex, 0, 0);
	SDL_RenderPresent(ren);

	return 0;
}

int
main(void)
{
	for (uint8_t r = 0; r < 6; r++)
		for (uint8_t g = 0; g < 6; g++)
			for (uint8_t b = 0; b < 6; b++)
				colormap[r*36 + g*6 + b] = r*0x33<<16 | g*0x33<<8 | b*0x33;

	int c;
	for (size_t i = 0; (c = getchar()) != EOF && i < sizeof(ram); i++)
		ram[i] = c;

	if (atexit(SDL_Quit) || SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO))
		return 1;

	win = SDL_CreateWindow("bytepusher", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, width*scale, height*scale, SDL_WINDOW_SHOWN);
	ren = SDL_CreateRenderer(win, -1, 0);
	tex = SDL_CreateTexture(ren, format, SDL_TEXTUREACCESS_STATIC, width, height);
	aud = SDL_OpenAudioDevice(0, 0, &audiospec, 0, SDL_AUDIO_ALLOW_ANY_CHANGE);
	SDL_PauseAudioDevice(aud, 0);

	if (!win || !ren || !tex || !aud)
		return 1;

	while (!cycle())
		;

	return 0;
}
