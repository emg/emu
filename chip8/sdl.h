extern size_t width, height;
int init(size_t width, size_t height, size_t scale);
void deinit(void);
void draw(char disp[height][width]);
int input(uint8_t *keys);
int waitinput(uint8_t *keys);
