#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>
#include <time.h>

#include "sdl.h"

enum {
	W        = 64,
	H        = 32,
	SCALE    = 10,
	FONTADDR = 0,
	HZ       = 500,
	NS       = (int)1E9,
	TIMERHZ  = 60,
};

typedef struct {
	uint16_t mask;
	uint16_t inst;
	void   (*func)(void);
} Inst;

char     disp[H][W];
uint8_t  V[0x10];
uint16_t PC;
uint16_t stack[0x10];
uint8_t  SP;
uint16_t I;
uint8_t  DT;
uint8_t  ST;
uint8_t  ram[0x1000] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0,
	0x20, 0x60, 0x20, 0x20, 0x70,
	0xF0, 0x10, 0xF0, 0x80, 0xF0,
	0xF0, 0x10, 0xF0, 0x10, 0xF0,
	0x90, 0x90, 0xF0, 0x10, 0x10,
	0xF0, 0x80, 0xF0, 0x10, 0xF0,
	0xF0, 0x80, 0xF0, 0x90, 0xF0,
	0xF0, 0x10, 0x20, 0x40, 0x40,
	0xF0, 0x90, 0xF0, 0x90, 0xF0,
	0xF0, 0x90, 0xF0, 0x10, 0xF0,
	0xF0, 0x90, 0xF0, 0x90, 0x90,
	0xE0, 0x90, 0xE0, 0x90, 0xE0,
	0xF0, 0x80, 0x80, 0x80, 0xF0,
	0xE0, 0x90, 0x90, 0x90, 0xE0,
	0xF0, 0x80, 0xF0, 0x80, 0xF0,
	0xF0, 0x80, 0xF0, 0x80, 0x80,
};

uint8_t  keys[0x10];
uint8_t  timer;
uint8_t  quit;

uint16_t inst;
uint16_t nnn;
uint8_t  n;
uint8_t  x;
uint8_t  y;
uint8_t  kk;

void cls  (void) { memset(disp, 0, sizeof(disp)); }
void ret  (void) { PC = stack[--SP]; }
void jp   (void) { PC = nnn; }
void call (void) { stack[SP++] = PC; PC = nnn; }
void sei  (void) { if (V[x] == kk) PC += 2; }
void snei (void) { if (V[x] != kk) PC += 2; }
void se   (void) { if (V[x] == V[y]) PC += 2; }
void ldi  (void) { V[x] = kk; }
void addi (void) { V[x] += kk; }
void ld   (void) { V[x] = V[y]; }
void or   (void) { V[x] |= V[y]; }
void and  (void) { V[x] &= V[y]; }
void xor  (void) { V[x] ^= V[y]; }
void add  (void) { V[x] += V[y]; V[0xF] = V[x] < V[y]; }
void sub  (void) { V[0xF] = V[x] >= V[y]; V[x] -= V[y]; }
void shr  (void) { V[0xF] = V[x] & 1; V[x] >>= 1; }
void subn (void) { V[0xF] = V[y] >= V[x]; V[x] = V[y] - V[x]; }
void shl  (void) { V[0xF] = V[x] >> 7; V[x] <<= 1; }
void sne  (void) { if (V[x] != V[y]) PC += 2; }
void ldIi (void) { I = nnn; }
void jpv0 (void) { PC = V[0] + nnn; }
void rnd  (void) { V[x] = rand() & kk; }
void skp  (void) { if (keys[V[x]]) PC += 2; }
void sknp (void) { if (!keys[V[x]]) PC += 2; }
void ldfDT(void) { V[x] = DT; }
void ldfK (void) { V[x] = waitinput(keys); if (V[x] == 0xFF) quit = 1;}
void ldDT (void) { DT = V[x]; }
void ldST (void) { ST = V[x]; }
void addI (void) { I = (I + V[x]) % 0xFFF; V[0xF] = I < V[x]; }
void ldF  (void) { I = FONTADDR + V[x]*5; }
void ldB  (void) { ram[I] = V[x]/100; ram[I+1] = V[x]/10%10; ram[I+2] = V[x]%10; }
void stoV (void) { for (uint8_t i = 0; i <= x; i++) ram[I+i] = V[i]; }
void ldV  (void) { for (uint8_t i = 0; i <= x; i++) V[i] = ram[I+i]; }
void nop  (void) {}

void
drw(void)
{
	V[0xF] = 0;
	for (int i = 0, py = V[y]%H; i < n; i++, py = (py+1) % H) {
		for (int j = 0, px = V[x]%W; j < 8; j++, px = (px+1) % W) {
			uint8_t old = disp[py][px];
			disp[py][px] ^= ram[I+i] >> 7-j & 1;
			V[0xF] |= old && !disp[py][px];
		}
	}
}

void
badi(void)
{
	fprintf(stderr, "bad instruction: inst:0x%04X PC:0x%04X\n", inst, PC-2);
	exit(1);
}

Inst insts[] = {
	{ 0xFFFF, 0x00E0, cls   },
	{ 0xFFFF, 0x00EE, ret   },
	{ 0xF000, 0x1000, jp    },
	{ 0xF000, 0x2000, call  },
	{ 0xF000, 0x3000, sei   },
	{ 0xF000, 0x4000, snei  },
	{ 0xF000, 0x5000, se    },
	{ 0xF000, 0x6000, ldi   },
	{ 0xF000, 0x7000, addi  },
	{ 0xF00F, 0x8000, ld    },
	{ 0xF00F, 0x8001, or    },
	{ 0xF00F, 0x8002, and   },
	{ 0xF00F, 0x8003, xor   },
	{ 0xF00F, 0x8004, add   },
	{ 0xF00F, 0x8005, sub   },
	{ 0xF00F, 0x8006, shr   },
	{ 0xF00F, 0x8007, subn  },
	{ 0xF00F, 0x800E, shl   },
	{ 0xF00F, 0x9000, sne   },
	{ 0xF000, 0xA000, ldIi  },
	{ 0xF000, 0xB000, jpv0  },
	{ 0xF000, 0xC000, rnd   },
	{ 0xF000, 0xD000, drw   },
	{ 0xF0FF, 0xE09E, skp   },
	{ 0xF0FF, 0xE0A1, sknp  },
	{ 0xF0FF, 0xF007, ldfDT },
	{ 0xF0FF, 0xF00A, ldfK  },
	{ 0xF0FF, 0xF015, ldDT  },
	{ 0xF0FF, 0xF018, ldST  },
	{ 0xF0FF, 0xF01E, addI  },
	{ 0xF0FF, 0xF029, ldF   },
	{ 0xF0FF, 0xF033, ldB   },
	{ 0xF0FF, 0xF055, stoV  },
	{ 0xF0FF, 0xF065, ldV   },
	{ 0xF000, 0x0000, nop   },
	{ 0x0000, 0x0000, badi  },
};

long
tsdiff(struct timespec new, struct timespec old)
{
	if (new.tv_nsec < old.tv_nsec) {
		new.tv_nsec += 1E9;
		new.tv_sec--;
	}
	return (new.tv_sec - old.tv_sec)*1E9 + new.tv_nsec - old.tv_nsec;
}

void
tick(struct timespec ts)
{
	static struct timespec lastdec;
	if (tsdiff(ts, lastdec) >= NS/TIMERHZ) {
		if (DT)
			--DT;
		if (ST) {
			/* TODO: make noise */
			--ST;
		}
		lastdec = ts;
	}

	inst = ram[PC]<<8 | ram[PC+1];
	nnn  =  inst & 0x0FFF;
	n    =  inst & 0x000F;
	x    = (inst & 0x0F00) >> 8;
	y    = (inst & 0x00F0) >> 4;
	kk   =  inst & 0x00FF;

	PC += 2;

	Inst *p;
	for (p = insts; (inst & p->mask) != p->inst; p++)
		;
	p->func();
}

int
main(void)
{
	if (atexit(deinit))
		return 1;

	if (init(W, H, SCALE))
		return 2;

	int c;
	PC = 0x200;
	while ((c = getchar()) != EOF)
		ram[PC++] = c;
	PC = 0x200;

	do {
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		tick(ts);
		draw(disp);
		nanosleep(&(struct timespec){.tv_nsec = 1E9/HZ}, 0); 
	} while (!input(keys) && !quit);

	return 0;
}
