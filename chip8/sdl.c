#include <SDL2/SDL.h>

#include "sdl.h"

size_t width, height;

static SDL_Window  *win;
static SDL_Surface *surf;
static size_t scale;

/*
 * Original:
 * 1 2 3 C
 * 4 5 6 D
 * 7 8 9 E
 * A 0 B F
 * 
 * Remapped:
 * 1 2 3 4
 * q w e r
 * a s d f
 * z x c v
 *
 * pad[original] = remapped
 */
static SDL_Keycode pad[] = {
	SDLK_x, SDLK_1, SDLK_2, SDLK_3,
	SDLK_q, SDLK_w, SDLK_e, SDLK_a,
	SDLK_s, SDLK_d, SDLK_z, SDLK_c,
	SDLK_4, SDLK_r, SDLK_f, SDLK_v,
};

int
init(size_t w, size_t h, size_t s)
{
	width  = w;
	height = h;
	scale  = s;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return -1;

	win = SDL_CreateWindow("CHIP8",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		width*scale, height*scale, SDL_WINDOW_SHOWN);
	if (!win)
		return -1;

	surf = SDL_GetWindowSurface(win);
	if (!surf)
		return -1;

	return 0;
}

void
deinit(void)
{
	if (surf)
		SDL_FreeSurface(surf);
	if (win)
		SDL_DestroyWindow(win);
	SDL_Quit();
}

void
draw(char disp[height][width])
{
	uint32_t black = SDL_MapRGB(surf->format, 0, 0, 0);
	uint32_t white = SDL_MapRGB(surf->format, 255,255,255);

	for (size_t i = 0; i < height; i++)
		for (size_t j = 0; j < width; j++) {
			SDL_FillRect(surf,
				&(SDL_Rect){.x = j*scale, .y = i*scale, .w = scale, .h = scale},
				disp[i][j] ? white : black);
				if (disp[i][j] && disp[i][j] != 1)
					fprintf(stderr, "bad pixel\n");
			}
	SDL_UpdateWindowSurface(win);
}

static int
keyevent(SDL_Event *ev, uint8_t *keys)
{
	for (size_t i = 0; i < sizeof(pad)/sizeof(*pad); i++) {
		if (ev->key.keysym.sym == pad[i]) {
			keys[i] = ev->type == SDL_KEYDOWN;
			return i;
		}
	}
	return -1;
}

int
input(uint8_t *keys)
{
	SDL_Event ev;
	while (SDL_PollEvent(&ev)) {
		if (ev.type == SDL_QUIT || (ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_ESCAPE))
			return 1;
		if (ev.type == SDL_KEYDOWN || ev.type == SDL_KEYUP)
			keyevent(&ev, keys);
	}
	return 0;
}

int
waitinput(uint8_t *keys)
{
	SDL_Event ev;
	int k = -1;
	while (SDL_WaitEvent(&ev)) {
		if (ev.type == SDL_QUIT || (ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_ESCAPE))
			break;
		if (ev.type == SDL_KEYDOWN && (k = keyevent(&ev, keys)) >= 0)
			break;
		if (ev.type == SDL_KEYUP)
			keyevent(&ev, keys);
	}
	return k;
}
